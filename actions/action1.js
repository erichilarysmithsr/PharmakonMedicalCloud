"use strict";
let datafire = require('datafire');

let reddit = require('@datafire/reddit').actions;
module.exports = new datafire.Action({
  handler: async (input, context) => {
    let result = await Promise.all([].map(item => reddit.wiki.pages.get({}, context)));
    return result;
  },
});
